yum install git epel-release -y
yum -y install python-pip 
pip install pep8 flake8 pyflakes isort yapf 
curl -o ~/.vimrc  https://gitlab.com/pbandark/config/raw/master/.vimrc
rm -rf ~/.vim/autoload/plug.vim
vim
